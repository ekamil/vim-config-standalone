" The magical turn-Vim-into-a-Python-IDE vim resource file!
"
" Mostly taken from http://www.sontek.net/category/Vim.aspx
" Other bits culled from various sources, Canonical guys, or made up by me.
"
" Julian Edwards 2008-05-30
" https://dev.launchpad.net/UltimateVimPythonSetup
"
" Removed some unused things. 2012-03-21
" Kamil Essekkat


" Python Options
setlocal foldlevelstart=99
setlocal foldlevel=99
setlocal foldmethod=indent

" Wrap at 72 chars for comments.
set formatoptions=cq textwidth=72 foldignore= wildignore+=*.py[co]


" Add the virtualenv's site-packages to vim path
py << EOF
import os.path
import sys
import vim
if 'VIRTUAL_ENV' in os.environ:
    project_base_dir = os.environ['VIRTUAL_ENV']
    sys.path.insert(0, project_base_dir)
    activate_this = os.path.join(project_base_dir, 'bin/activate_this.py')
    execfile(activate_this, dict(__file__=activate_this))
EOF


" `gf` jumps to the filename under the cursor.  Point at an import statement
" and jump to it!
python << EOF
import os
import sys
import vim
for p in sys.path:
    if os.path.isdir(p):
        vim.command(r"set path+=%s" % (p.replace(" ", r"\ ")))
EOF

function! VEnvMakePrg()
    if &ft != 'python'
        return
    endif
    if exists("g:virtualenv_loaded") && exists("g:virtualenv_name")
        let mkpath = g:virtualenv_directory . '/' . g:virtualenv_name . '/bin/makeprg'
        if executable(mkpath)
            " echo 'Using ' . mkpath . ' as makeprg'
            let &makeprg = mkpath
            return
        endif
    endif
    if !empty($VIM_MAKEPRG)
        " echo 'Using >>' . $VIM_MAKEPRG . '<< from env as makeprg'
        let &makeprg = $VIM_MAKEPRG
        return
    endif
endfunction

call VEnvMakePrg()
