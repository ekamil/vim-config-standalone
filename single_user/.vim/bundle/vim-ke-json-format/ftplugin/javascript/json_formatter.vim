" Format json
python << EOL
import vim
import json

def pretty(instr):
    """@returns: String made pretty by json
    """
    parsed = json.loads(instr)
    return json.dumps(parsed,
                      sort_keys=True,
                      indent=4,
                      separators=(',', ': '))


def prettyJSON_flesh(inout_object):
    """Modifies inout_object: current.buffer or current.range,
    or any other list of strings.
    """
    x = pretty('\n'.join(inout_object))
    if x:
        inout_object[:] = None
    for lin in x.splitlines():
        try:
            inout_object.append("%s" % lin)
        except:
            print "Erroneous line: %s " % lin
            inout_object.append("DELETED BECAUSE OF ENCODING ERRORS")


def prettyJSON_buffer():
    prettyJSON_flesh(vim.current.buffer)


def prettyJSON_range():
    prettyJSON_flesh(vim.current.range)


EOL

nmap <C-f> :py prettyJSON_buffer()<CR>
vmap <C-f> :py prettyJSON_range()<CR>
