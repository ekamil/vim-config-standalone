" Format xml
python << EOL
import vim

def pretty_a(instr):
    """@returns: String made pretty by xml.dom.minidom
    """
    import xml.dom.minidom as dom
    import re
    instr = dom.parseString(instr)
    instr = instr.toprettyxml(indent='    ', encoding='utf-8')
    text_re = re.compile('>\n\s+([^<>\s].*?)\n\s+</', re.DOTALL)
    instr = text_re.sub('>\g<1></', instr)
    print "Using xml.dom.minidom"
    return '\n'.join([s for s in instr.splitlines() if s.strip()])


def pretty_b(instr):
    """@returns: String made pretty by xml.dom.ext
    @warn: Untested
    """
    from xml.dom.ext import PrettyPrint
    from StringIO import StringIO
    tmpStream = StringIO()
    PrettyPrint(instr, stream=tmpStream, encoding='utf-8')
    print "Using xml.dom.ext"
    return tmpStream.getvalue()


def pretty_c(instr):
    """@returns: String made pretty by lxml.etree
    @warn: Unicode chars are escaped. Dont know why
    """
    import lxml.etree as etree
    _parser = etree.XMLParser(remove_blank_text=True,
                                encoding='utf-8',
                                strip_cdata=False)
    x = etree.fromstring(instr, parser=_parser)
    print "Using lxml"
    return etree.tostring(x, pretty_print=True, )


def prettyXML_flesh(inout_object):
    """Modifies inout_object: current.buffer or current.range,
    or any other list of strings.
    """
    try:
        x = pretty_c('\n'.join(inout_object))
    except ImportError:
        try:
            x = pretty_b('\n'.join(inout_object))
        except ImportError:
            x = pretty_a('\n'.join(inout_object))
    if x:
        inout_object[:] = None
    for lin in x.splitlines():
        try:
            inout_object.append("%s" % lin)
        except:
            print "Errornous line: %s " % lin
            inout_object.append("DELETED BECAUSE OF ENCODING ERRORS")


def prettyXML_buffer():
    prettyXML_flesh(vim.current.buffer)


def prettyXML_range():
    prettyXML_flesh(vim.current.range)


EOL

nmap <C-f> :py prettyXML_buffer()<CR>
vmap <C-f> :py prettyXML_range()<CR>
