VIMBASE =../essi-vim-config/files/
VERSION = $(shell cd $(VIMBASE) && git log -1 --pretty=%h)
MR = mr -q -j 4
WIN_TMP = win_vim
SU_TMP = single_user

# TODO = $(WIN_TMP)_$(VERSION).7z $(SU_TMP)_$(VERSION).7z
TODO =  $(SU_TMP)_$(VERSION).7z

all: clean $(TODO)

	echo $(VERSION) > VERSION
	$(MAKE) rmgit


$(WIN_TMP):
	mkdir -p $(WIN_TMP)/vimfiles
	cp $(VIMBASE)/vimrc $(WIN_TMP)/_vimrc
	rsync -a $(VIMBASE)/vim74/ $(WIN_TMP)/vimfiles/
	cd $(WIN_TMP)/vimfiles && $(MR) up
	cd $(WIN_TMP)/vimfiles && $(MR) gc
	rm $(WIN_TMP)/vimfiles/.gitignore


$(WIN_TMP)_$(VERSION).7z: $(WIN_TMP)
	cd $(WIN_TMP) && 7z a ../$(WIN_TMP)_$(VERSION).7z vimfiles _vimrc > /dev/null


$(SU_TMP):
	mkdir -p $(SU_TMP)/.vim
	rsync -a $(VIMBASE)/vim74/ $(SU_TMP)/.vim/
	cp $(VIMBASE)/vimrc $(SU_TMP)/.vim/vimrc
	cd $(SU_TMP)/.vim && $(MR) up
	cd $(SU_TMP)/.vim && $(MR) gc
	rm $(SU_TMP)/.vim/.gitignore


$(SU_TMP)_$(VERSION).7z: $(SU_TMP)
	7z a $(SU_TMP)_$(VERSION).7z $(SU_TMP) > /dev/null


7z: $(SU_TMP)_$(VERSION).7z $(WIN_TMP)_$(VERSION).7z

clean:
	rm -rf $(SU_TMP) $(WIN_TMP) *7z

rmgit:
	find $(SU_TMP) $(WIN_TMP) -type d -name .git | xargs rm -rf

install:
	rsync -av single_user/.vim/ ~/.vim/ --delete-before
